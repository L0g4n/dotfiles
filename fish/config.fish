alias vim nvim
alias grep rg
alias find fd
alias cat bat

fish_vi_key_bindings

# remove the greeting message
set fish_greeting

# set fish env vars
set -Ux IDF_PATH $HOME/esp/esp-idf

# starfish prompt
starship init fish | source
