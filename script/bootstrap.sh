#!/bin/sh

[ $(uname) = "Darwin" ] && macos=1 || macos=0

setup_vim() {
  mkdir -p ~/.config/nvim/tmp
  mkdir -p ~/.config/nvim/tmp/backup
  mkdir -p ~/.config/nvim/tmp/undo

  echo "Vim ✓"
}

setup_tmux() {
  if [ ! -d ~/.tmux/plugins/tpm ]; then
    git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
  fi

  echo "tmux ✓"
}



check_for_homebrew() {
  if [ $(uname) = "Darwin" ]; then
    if !(which brew > /dev/null)
    then
      echo "Install homebrew before continuing"
      exit 1
    fi

    echo "homebrew ✓"
  fi
}

script/setup_symlinks.sh
setup_vim
check_for_homebrew
setup_tmux

[ $macos = 1 ] && echo "source ~/.zshrc.local" >> $HOME/.zshrc
[ $macos = 0 ] && echo "source ~/.bashrc.local" >> $HOME/.bashrc
