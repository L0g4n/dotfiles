#!/bin/sh

[ $(uname) = "Darwin" ] && macos=1 || macos=0

setup_symlinks() {
	for source in `find ~/dotfiles -iname "*.symlink"`
	do
		dest="$HOME/.`basename \"${source%.*}\"`"
		rm -rf $dest
		ln -s $source $dest
	done
	
	mkdir -p $HOME/.config/alacritty
	rm -rf $HOME/.config/nvim
	rm -rf $HOME/.config/fish

	# vim symlinks 
	ln -s $HOME/dotfiles/nvim $HOME/.config/nvim
	ln -s $HOME/dotfiles/nvim/ $HOME/.vim

	ln -s $HOME/dotfiles/fish $HOME/.config/fish
	[ $macos = 1 ] && ln -s $HOME/dotfiles/bash/bashrc $HOME/.zshrc.local
	[ $macos = 0 ] && ln -s $HOME/dotfiles/bash/bashrc $HOME/.bashrc.local

	ln -s $HOME/dotfiles/alacritty/config.toml $HOME/.config/alacritty/alacritty.toml
	ln -s $HOME/dotfiles/starship/config.toml $HOME/.config/starship.toml

	ln -s $HOME/dotfiles/tmux/tmux.conf $HOME/.tmux.conf

	echo "symlinks ✓"
}

setup_symlinks
