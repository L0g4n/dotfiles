# dotfiles

Run `script/bootstrap.sh` to set everything up.

Note: Map the **caps lock** to the **escape** key in your OS, to make switching between normal and interactive mode in vim a lot easiert.

Use `:PackerUpdate` in vim to install the plugins, `Prefix+I` to install the tmux plugins. In my case, my `Prefix` keyboard shortcut for tmux is Control+Space.

